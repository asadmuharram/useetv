<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Verifikasi extends CI_Controller {

 function __construct()
 {
   parent::__construct();
    $this->load->model('user','',TRUE);
    $this->load->model('cdr','',TRUE);
    $this->load->helper('url');
    $this->load->helper('form');
 }

 function index() {
    $roleUser = $this->session->userdata('logged_in');
    $userVer = $this->input->get('user');
    $verCode = $this->input->get('ver');

    $cekVerStatus = $this->user->cekVerStatus($userVer);
    foreach ($cekVerStatus as $row) {
        $verStatusDB = $row->verification_status;
    }
    $statusVer = MD5('d');
    if ($verStatusDB == $statusVer) {
      $this->load->view('verifikasi_done_fail_view');
    } else {
      $cekVer = $this->user->cekVerCode($userVer,$verCode);
      if ($cekVer) {
      $statusCharD = 'd';
      $verStatusD = MD5($statusCharD);
      $data = array(
      'status_user' => "active",
      'verification_status' => $verStatusD,
      );
      $this->user->updateStatusUser($userVer,$data);
      $this->load->view('verifikasi_success_view');
      } else {
      $this->load->view('verifikasi_fail_view');
      }
    }
  
  }

}