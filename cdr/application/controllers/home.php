<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
    $this->load->model('user','',TRUE);
    $this->load->model('cdr','',TRUE);
    $this->load->helper('url');
    $this->load->helper('form');
 }

 function index()
 {
    if($this->session->userdata('logged_in'))
    {
      $roleUser = $this->session->userdata('logged_in');
      $indexUser = $roleUser['id'];
      $roleUserA = $roleUser['role'];
      if ($roleUserA == 1) {
           redirect('admin', 'refresh');
      } else {
          $data['user'] = $this->user->getUserByID($indexUser);
          $data['userNameLogin'] = $roleUser['username'];
          $this->load->view('template/header_view', $data);
          $this->load->view('home_view', $data);
          $this->load->view('template/footer_view', $data);
      } 
    }
      else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

 function search() {
  if($this->session->userdata('logged_in'))
    {
    $roleUser = $this->session->userdata('logged_in');
    $indexUser = $roleUser['id'];
    $idiptv = $this->input->get('idiptv');
    $tglTbl = $this->input->get('tgl');
    switch ($tglTbl) {
      case 'daily':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblDaily($idiptv);
        $data['tgl'] = date('d / m / Y');
        break;
      case 'h1':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH1($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-1 days"));
        break;
      case 'h2':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH2($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-2 days"));
        break;
      case 'h3':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH3($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-3 days"));
        break;
      case 'h4':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH4($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-4 days"));
        break;
      case 'h5':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH5($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-5 days"));
        break;
      case 'h6':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH6($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-6 days"));
        break;
      case 'h7':
        $data['cdrStatus'] = $this->cdr->getTblInfo($tglTbl);
        $data['cdrData'] = $this->cdr->getAllDataByIdTblH7($idiptv);
        $data['tgl'] = date('d / m / Y',strtotime("-7 days"));
        break;
    }
    $data['userNameLogin'] = $roleUser['username'];
    $data['user'] = $this->user->getUserByID($indexUser);
    $this->load->view('template/header_view', $data);
    $this->load->view('search_view', $data);
    $this->load->view('template/footer_view', $data);
  } else {
    //If no session, redirect to login page
     redirect('login', 'refresh');
  }
}

}

?>