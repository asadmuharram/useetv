<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Registrasi extends CI_Controller {
function __construct()
{
parent::__construct();
$this->load->model('user','',TRUE);
$this->load->helper('url');
$this->load->helper('form');
}
function index()
{
  $data['title'] = 'Registrasi CDR';
	$this->load->view('registrasi_view',$data);
}

function randomString() {
    $length = 6;
    $str = "";
    $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
    $max = count($characters) - 1;
    for ($i = 0; $i < $length; $i++) {
    $rand = mt_rand(0, $max);
    $str .= $characters[$rand];
    }
    return $str;
}

function sendMail($usernameUser,$verCode,$emailUser)
{
$config = Array(
  'protocol' => 'smtp',
  'smtp_host' => 'ssl://smtp.googlemail.com',
  'smtp_port' => 465,
  'smtp_user' => 'avatar.itss@gmail.com', // change it to yours
  'smtp_pass' => 'itss2015', // change it to yours
  'mailtype' => 'html',
  'charset' => 'iso-8859-1',
  'wordwrap' => FALSE
);

      $message = "Terima kasih sudah mendaftar akun CDR Avatar ITSS <br/>";
      $message .= "Untuk mengaktifkan akun anda , silahkan klik link dibawah ini. <br/>";
      $message .= "<a href='http://10.11.32.27/cdr/verifikasi?user=".$usernameUser."&ver=".$verCode."'>Verifikasi Akun</a>";
      $message .= " * pastikan koneksi anda terhubung dengan jaringan telkom <br/>";
      $this->load->library('email', $config);
      $this->email->set_newline("\r\n");
      $this->email->from('avatar.itss@gmail.com'); // change it to yours
      $this->email->to('asadmuharram@gmail.com');// change it to yours
      $this->email->subject('Verifikasi Akun CDR');
      $this->email->message($message);
      if($this->email->send())
      {
        return true;
      }
      else
      {
        return false;
      }
}

function create(){

    $this->load->library('form_validation');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
    $this->form_validation->set_rules('nama', 'Nama Lengkap', 'trim|required|xss_clean');
    $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]');
    $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
    $this->form_validation->set_rules('tlp', 'Telepon', 'trim|required|xss_clean');
    if ($this->form_validation->run() == FALSE) {
      //Field validation failed.  User redirected to login page
      $this->load->view('registrasi_view');
    } else {
      //Go to private area
      $roleUser = '3';
      $statusUser = 'inactive';
      $namaUser = $this->input->post('nama');
      $usernameUser = $this->input->post('username');
      $emailname = $this->input->post('email');
      $emailUser = $emailname."@telkom.co.id";
      $pwdUser = $this->input->post('password');
      $tlpUser = $this->input->post('tlp');
      $dateNow = date('Y-m-d H:i:s');
      $verCode = MD5($this->randomString());
      $statusChar = 'p';
      $verStatus = MD5($statusChar);
      if ($this->user->cekUsername($usernameUser)) {
        if ($this->user->cekEmail($emailUser)) {
        if ($this->sendMail($usernameUser,$verCode,$emailUser)) {
            $result = $this->user->insertUser($namaUser,$usernameUser,$emailUser,$pwdUser,$tlpUser,$statusUser,$roleUser,$dateNow,$verCode,$verStatus);
            $this->load->view('registrasi_success_view');
        } else {
             $this->load->view('sendemail_fail_view');
        }
         } else {
        $this->load->view('registrasi_email_fail_view');
      }
      }
      else {
        $this->load->view('registrasi_username_fail_view');
      }
      }
}

}
?>