<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Usermanage extends CI_Controller {

 function __construct()
 {
   parent::__construct();
    $this->load->model('user','',TRUE);
    $this->load->model('cdr','',TRUE);
    $this->load->helper('url');
    $this->load->helper('form');
 }

 function index()
 {
    if($this->session->userdata('logged_in'))
    {
      $roleUser = $this->session->userdata('logged_in');
      $indexUser = $roleUser['id'];
      $roleUserA = $roleUser['role'];
      if ($roleUserA == 1) {
      $data['user'] = $this->user->getUserByID($indexUser);
      $data['AllUser'] = $this->user->getAllUser();
      $data['title'] = 'User Management';
      $data['userNameLogin'] = $roleUser['username'];
      $this->load->view('template/admin_header_view', $data);
      $this->load->view('admin_usermanage_index_view', $data);
      $this->load->view('template/footer_view', $data);
      } else {
        //If no session, redirect to login page
        redirect('login', 'refresh');
      }
    }
      else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }
 }

 function edit($id)
 {
    if($this->session->userdata('logged_in'))
    {
      $roleUser = $this->session->userdata('logged_in');
      $indexUser = $roleUser['id'];
      $roleUserA = $roleUser['role'];
      if ($roleUserA == 1) {
      $data['userNameLogin'] = $roleUser['username'];
      $data['user'] = $this->user->getUserByID($id);
      $data['title'] = 'User Management';
      $this->load->view('template/admin_header_view', $data);
      $this->load->view('admin_usermanage_edit_view', $data);
      $this->load->view('template/footer_view', $data);
      } else {
          //If no session, redirect to login page
          redirect('login', 'refresh');
      }
    }
      else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }
 }

}

?>