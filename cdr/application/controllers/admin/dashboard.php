<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Dashboard extends CI_Controller {

 function __construct()
 {
   parent::__construct();
    $this->load->model('user','',TRUE);
    $this->load->model('cdr','',TRUE);
    $this->load->helper('url');
    $this->load->helper('form');
 }

 function index()
 {
    if($this->session->userdata('logged_in'))
    {
      $roleUser = $this->session->userdata('logged_in');
       $indexUser = $roleUser['id'];
      $roleUserA = $roleUser['role'];
      if ($roleUserA == 1) {
          $data['user'] = $this->user->getUserByID($indexUser);
          $data['title'] = 'Dashboard - Admin';
          $data['userNameLogin'] = $roleUser['username'];
          $this->load->view('template/admin_header_view', $data);
          $this->load->view('admin_view', $data);
          $this->load->view('template/footer_view', $data);
      } else {
          redirect('home', 'refresh');
      } 
    }
      else
    {
     //If no session, redirect to login page
     redirect('login', 'refresh');
    }
 }

 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('home', 'refresh');
 }

}

?>