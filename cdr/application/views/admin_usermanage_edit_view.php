<?php foreach ($user as $row) {
}
?>
<div class="container">
         <div class="col-md-12">
            <div class="page-header">
                  <h3>Edit User</h3>
            </div>
            <br/>
            <form class="form-horizontal">
<fieldset>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="namauser">Nama Lengkap</label>  
  <div class="col-md-4">
  <input id="namauser" name="namauser" type="text" placeholder="" class="form-control input-md" required="" value="<?=$row->nama_user?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="username">Username</label>  
  <div class="col-md-4">
  <input id="username" name="username" type="text" placeholder="" class="form-control input-md" value="<?=$row->username_user?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="" class="form-control input-md" value="<?=$row->email_user?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="notlp">No. Telepon</label>  
  <div class="col-md-4">
  <input id="notlp" name="notlp" type="text" placeholder="" class="form-control input-md" value="<?=$row->no_tlp_user?>">
    
  </div>
</div>

<!-- Multiple Checkboxes -->
<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Status User</label>
  <div class="col-md-4">
    <?php
         if ($row->status_user == 'active') {
            ?>
            <div class="radio">
    <label for="radios-0">
      <input type="radio" name="statusUser" id="radios-0" value="active" checked="checked">
      Active
    </label>
   </div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="statusUser" id="radios-1" value="inactive">
      Inactive
    </label>
   </div>
            <?php
         } else {
            ?>
            <div class="radio">
    <label for="radios-0">
      <input type="radio" name="statusUser" id="radios-0" value="active">
      Active
    </label>
   </div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="statusUser" id="radios-1" value="inactive" checked="checked">
      Inactive
    </label>
   </div>
            <?php
         }
      ?>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="roleuser">Role</label>
  <div class="col-md-4">
    <select id="roleuser" name="roleuser" class="form-control">
      <option value="">- Pilih Role -</option>
      <option value="1">Superadmin</option>
      <option value="2">Administrator</option>
      <option value="3">User</option>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="roleuser">Hapus Akun Ini ?</label>
  <div class="col-md-4">
    <a href="<?=base_url('admin/usermanage/delete/'.$row->index_user)?>"><button id="hapusakun" class="btn btn-primary btn-sm">Ya</button></a>
  </div>
</div>
<div class="row"></div>
<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-8">
    <button id="submit" name="submit" class="btn btn-info">Simpan</button>
    <button id="reset" name="reset" class="btn btn-danger" onclick="history.back(-1)">Cancel</button>
  </div>
</div>
</fieldset>
</form>

            <!--<div class='form-row'>
              <div class='col-xs-4 form-group cvc required'>
                <label class='control-label'>CVC</label>
                <input autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text'>
              </div>
              <div class='col-xs-4 form-group expiration required'>
                <label class='control-label'>Expiration</label>
                <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
              </div>
              <div class='col-xs-4 form-group expiration required'>
                <label class='control-label'> </label>
                <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
              </div>
            </div>-->
            </div>
         </div>
   </div>
   <div class="row"></div>