<div class="container">
         <div class="col-md-12">
            <div class="page-header">
                  <h3>User Management</h3>
            </div>
            <div class="tblInfo">
               <input type="button" value="<< Back" onclick="history.back(-1)" class="btn btn-default pull-left">
            </div>
            <div class="row"></div>
            <br/>
            <table class="table table-bordered">
               <thead>
                  <th>No.</th>
                  <th>Nama User</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>No. Tlp</th>
                  <th>Role</th>
                  <th>Status User</th>
                  <th>Status Verifikasi</th>
                  <th>Tanggal Daftar</th>
                  <th>Action</th>
               </thead>
               <tbody>
                  <?php
                  $no = 1;
                  foreach ($AllUser as $row) {
                  echo "<tr>";
                  echo "<td>".$no."</td>";
                  echo "<td>".$row->nama_user."</td>";
                  echo "<td>".$row->username_user."</td>";
                  echo "<td>".$row->email_user."</td>";
                  echo "<td>".$row->no_tlp_user."</td>";
                  switch ($row->role_user) {
                     case 1 :
                        $roleName = 'Superadmin';
                        echo "<td>".$roleName."</td>";
                        break;
                     case 2 :
                        $roleName = 'Administrator';
                        echo "<td>".$roleName."</td>";
                        break;
                     case 3 :
                        $roleName = 'User';
                        echo "<td>".$roleName."</td>";
                        break;
                  }
                  switch ($row->status_user) {
                     case "active":
                        echo "<td><span class='label label-primary' style='font-size:10px'>".$row->status_user."</span></td>";
                        break;
                     case "inactive":
                        echo "<td><span class='label label-danger' style='font-size:10px'>".$row->status_user."</span></td>";
                        break;
                  }
                  switch ($row->verification_status) {
                     case "8277e0910d750195b448797616e091ad":
                        $verificationStatus = 'Done';
                        echo "<td><span class='label label-success' style='font-size:10px'>".$verificationStatus."</span></td>";
                        break;
                     case "83878c91171338902e0fe0fb97a8c47a":
                        $verificationStatus = 'Pending';
                        echo "<td><span class='label label-warning' style='font-size:10px'>".$verificationStatus."</span></td>";
                        break;
                  }
                  echo "<td>".date('H:i:s d/m/Y',strtotime($row->date_created))."</td>";
                  ?>
                  <td><a href="<?=base_url('admin/usermanage/edit/'.$row->index_user)?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a> |
                  <a href="<?=base_url('admin/usermanage/delete/'.$row->index_user)?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a></td>
                  <?php
                  echo "</tr>";
                  $no++;
                  }
                  ?>
               </tbody>
            </table>
         </div>
   </div>
   <div class="row"></div>