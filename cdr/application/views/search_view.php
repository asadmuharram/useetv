<div class="container">
         <div class="col-md-12">
            <div class="page-header">
                  <h3>Aktifitas Pemakaian <small>per<?php echo "tanggal : ".$tgl;?></small></h3>
            </div>
            <div class="tblInfo">
               <input type="button" value="<< Back" onclick="history.back(-1)" class="btn btn-default pull-left">
                <h5 class="pull-right">
                  <b>Data Status :</b>
                  <?php foreach ($cdrStatus as $row) {
                     if ($row->tbl_status == "completed") {
                        echo "<span class='label label-success'>Completed</span>";
                     } else {
                        echo "<span class='label label-primary'>Transferring</span>";
                     }
                  }
                  ?>
                  <span class="label label-success"></span>
               </h5>
            </div>
            <div class="row"></div>
            <br/>
            <table class="table table-bordered">
               <thead>
                  <th>ID IPTV</th>
                  <th>Begin Time</th>
                  <th>End Time</th>
                  <th>Ip Address</th>
                  <th>STB ID</th>
                  <th>Channel</th>
                  <th>Tipe</th>
               </thead>
               <tbody>
                  <?php
                  foreach ($cdrData as $row) {
                  echo "<tr>";
                  echo "<td>".$row->cardno."</td>";
                  echo "<td>".date('H:i:s d/m/Y',strtotime($row->begintime))."</td>";
                  echo "<td>".date('H:i:s d/m/Y',strtotime($row->endtime))."</td>";
                  echo "<td>".$row->param18."</td>";
                  echo "<td>".$row->param40."</td>";
                  echo "<td>".$row->param4."</td>";
                  if ($row->param20 == '') {
                     $tipe = 'live-tv';
                  } else {
                     $tipe = 'vod';
                  }
                  echo "<td>".$tipe."</td>";
                  echo "</tr>";
                  }
                  ?>
               </tbody>
            </table>
         </div>
   </div>
   <div class="row"></div>