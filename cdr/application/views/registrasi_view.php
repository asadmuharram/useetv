<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="http://10.11.32.27/binding/images/icom.ico">
    <title><?=($title)?></title>
    <!-- Bootstrap -->
    <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div id="signupbox" style="margin-top:50px" class="mainbox col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
        <?php echo validation_errors(); ?>
        <div class="panel panel-info">
          <div class="panel-heading">
            <div class="panel-title">Daftar Akun</div>
          </div>
          <div class="panel-body" >
            <?php echo form_open('registrasi/create', 'class=form-horizontal');?>
              <div class="form-group">
                <label for="email" class="col-md-3 control-label">Nama Lengkap</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="nama">
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-3 control-label">Username</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="username">
                </div>
              </div>
              <div class="form-group">
                <label for="firstname" class="col-md-3 control-label">Alamat Email</label>
                <div class="col-md-9">
                <div class="input-group">
                <input type="text" class="form-control" id="exampleInputAmount" name="email">
                <div class="input-group-addon">@telkom.co.id</div>
                </div>
                <div style="font-size:11px;margin-top:5px">* hanya email @telkom.co.id</div>
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-3 control-label">Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="password">
                </div>
              </div>
              <div class="form-group">
                <label for="password" class="col-md-3 control-label">Konfirmasi Password</label>
                <div class="col-md-9">
                  <input type="password" class="form-control" name="passconf">
                </div>
              </div>
              <div class="form-group">
                <label for="icode" class="col-md-3 control-label">No. Telepon</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="tlp">
                </div>
              </div>
              <div class="form-group">
                <!-- Button -->
                <div class="col-md-offset-3 col-md-9">
                  <input type="submit" class="btn btn-success" value="Daftar">
                  <input type="reset" class="btn btn-primary" value="Reset">
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-12 control">
                  <div style="border-top: 1px solid#888; padding-top:15px;margin-top:15px; font-size:85%" >
                    Sudah Punya Akun ?<a href="<?=base_url('login')?>"> Login</a>
                  </div>
                </div>
              </div>
            <?php echo form_close();?>
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=base_url('assets/js/bootstrap.min.js')?>"></script>
  </body>
</html>