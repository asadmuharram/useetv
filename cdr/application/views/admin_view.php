<!-- Content -->
   <div class="container">
      <div class="row">
         <div class="col-md-6 col-sm-6 col-md-offset-3">
            <div class="panel panel-default">
            <form action="home/search" method="get" form-horizontal>
            <div class="panel panel-body">
            <div class="form-group">
               <label for="exampleInputEmail1">ID IPTV</label>
               <input type="text" class="form-control" id="exampleInputEmail1" name="idiptv">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Per Tanggal</label>
               <select name="tgl" class="form-control">
                  <option value="">== Pilih Tanggal ==</option>
                  <?php
                  echo "<option value='daily'>".date('d / m / Y')."</option>";
                  echo "<option value='h1'>".date('d / m / Y',strtotime("-1 days"))."</option>";
                  echo "<option value='h2'>".date('d / m / Y',strtotime("-2 days"))."</option>";
                  echo "<option value='h3'>".date('d / m / Y',strtotime("-3 days"))."</option>";
                  echo "<option value='h4'>".date('d / m / Y',strtotime("-4 days"))."</option>";
                  echo "<option value='h5'>".date('d / m / Y',strtotime("-5 days"))."</option>";
                  echo "<option value='h6'>".date('d / m / Y',strtotime("-6 days"))."</option>";
                  echo "<option value='h7'>".date('d / m / Y',strtotime("-7 days"))."</option>";
                  ?>
               </select>
            </div>
            <input type="submit" class="btn btn-success" value="Submit">
            </div>
         </form>
         </div>
      </div>
   </div>
</div>