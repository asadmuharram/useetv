<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="http://10.11.32.27/binding/images/icom.ico">
      <title>IPTV CDR</title>
      <!-- Bootstrap -->
      <link href="<?=base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
      <link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- navbar -->
      <nav class="navbar navbar-red  navbar-static-top" role="navigation">
         <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
               <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
               class="icon-bar"></span><span class="icon-bar"></span>
               </button>
               <a class="navbar-brand"><img src="http://10.11.32.27/binding/images/smalllogo.png"></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
               <ul class="nav navbar-nav">
                  <li><a href="<?=base_url('/home')?>">Home</a></li>
               </ul>
               <ul class="nav navbar-nav navbar-right">
                  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Selamat Datang , <b><?php echo ucfirst($userNameLogin);?></b> <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                     <!--<li><a onClick="window.open('setting.php','Windowname','width=455,height=320');" href="#">Setting</a></li>-->
                     <!--<li class="divider"></li>-->
                     <li><a href="<?=base_url('home/logout')?>">Logout</a></li>
                  </ul>
               </li>
            </ul>
         </div>
      </div>
   </nav>
   <!-- /.navbar -->
   <!-- Content -->