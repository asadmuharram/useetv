<?php
Class User extends CI_Model {

	function login($username, $password, $status) {
		$this->db->select('index_user, username_user,password_user,status_user,role_user');
		$this->db->from('tbl_users');
		$this->db->where('username_user', $username);
		$this->db->where('password_user', MD5($password));
		$this->db->where('status_user', $status);
		$this->db->limit(1);
		$query = $this->db->get();
		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getUser() {
		$this->db->select('index_user,username_user,status_user,role_user');
		$this->db->from('tbl_users');
		$query = $this->db->get();
		return $query->result();
	}
	function getAllUser() {
		$this->db->select('index_user,nama_user,username_user,email_user,no_tlp_user,status_user,role_user,date_created,verification_status');
		$this->db->from('tbl_users');
		$query = $this->db->get();
		return $query->result();
	}
	function getUserByID($indexUser) {
		$this->db->select('index_user,nama_user,username_user,email_user,no_tlp_user,status_user,role_user,date_created,verification_status');
		$this->db->from('tbl_users');
		$this->db->where('index_user', $indexUser);
		$query = $this->db->get();
		return $query->result();
	}
	function insertUser($namaUser,$usernameUser,$emailUser,$pwdUser,$tlpUser,$statusUser,$roleUser,$dateNow,$verCode,$verStatus)
    {
        $this->nama_user   		= $namaUser; // please read the below note
        $this->username_user 	= $usernameUser;
        $this->email_user    	= $emailUser;
        $this->password_user    = MD5($pwdUser);
        $this->no_tlp_user    	= $tlpUser;
        $this->status_user    	= $statusUser;
        $this->role_user   		= $roleUser;
        $this->date_created   	= $dateNow;
        $this->verification_code  = $verCode;
        $this->verification_status  = $verStatus;
        $this->db->insert('tbl_users', $this);
    }
    function cekUsername($usernameUser) {
		$this->db->select('username_user');
		$this->db->from('tbl_users');
		$this->db->where('username_user', $usernameUser);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	function cekEmail($emailUser) {
		$this->db->select('email_user');
		$this->db->from('tbl_users');
		$this->db->where('email_user', $emailUser);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return false;
		} else {
			return true;
		}
	}
	function cekVerStatus($userVer) {
		$this->db->select('username_user,verification_status');
		$this->db->from('tbl_users');
		$this->db->where('username_user', $userVer);
		$query = $this->db->get();
		return $query->result();
	}
	function cekVerCode($userVer,$verCode) {
		$this->db->select('username_user');
		$this->db->from('tbl_users');
		$this->db->where('username_user', $userVer);
		$this->db->where('verification_code', $verCode);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	function updateStatusUser($username,$data){
		$this->db->where('username_user', $username);
		$this->db->update('tbl_users', $data);
	}
}
?>