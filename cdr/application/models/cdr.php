<?php
Class Cdr extends CI_Model {

	function getAllDataByIdTblDaily($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_daily');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH1($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h1');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH2($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h2');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH3($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h3');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH4($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h4');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH5($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h5');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH6($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h6');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getAllDataByIdTblH7($idiptv) {
		$this->db->select('cardno,begintime,endtime,param18,param20,param4,param40');
		$this->db->from('tbl_cdr_file_data_h7');
		$this->db->where('cardno', $idiptv);
		$this->db->order_by("begintime", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	function getTblInfo($tglTbl) {
		$this->db->select('tbl_status,tbl_name');
		$this->db->from('tbl_info_process');
		$this->db->where('tbl_name', $tglTbl);
		$query = $this->db->get();
		return $query->result();
	}

	/*function getAllDataByIdToday($idiptv) {
		$this->db->select('cardno,begintime,param18,param20,param4');
		$this->db->from('tbl_cdr_file_data_tmp');
		$this->db->where('cardno', $idiptv);
		$this->db->where('DATE(begintime) = DATE(NOW()) - INTERVAL 1 DAY AND DATE(NOW()) LIMIT 10');
		$query = $this->db->get();
		return $query->result();
	}
	function getAllDataByIdYesterday($idiptv) {
		$this->db->select('cardno,begintime,param18,param20,param4');
		$this->db->from('tbl_cdr_file_data_tmp');
		$this->db->where('cardno', $idiptv);
		$this->db->where('begintime BETWEEN DATE(NOW()) - INTERVAL 2 DAY AND DATE(NOW()) LIMIT 10');
		$query = $this->db->get();
		return $query->result();
	}
	function getAllDataByIdBeforeYesterday($idiptv) {
		$this->db->select('cardno,begintime,param18,param20,param4');
		$this->db->from('tbl_cdr_file_data_tmp');
		$this->db->where('cardno', $idiptv);
		$this->db->where('begintime BETWEEN DATE(NOW()) - INTERVAL 3 DAY AND DATE(NOW()) - INTERVAL 1 DAY LIMIT 10');
		$query = $this->db->get();
		return $query->result();
	}*/
}
?>